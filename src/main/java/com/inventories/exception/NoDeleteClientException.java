package com.inventories.exception;

public class NoDeleteClientException extends Exception {

	private static final long serialVersionUID = -8289300744303871297L;

	public NoDeleteClientException(String msg, Exception e) {
		super(msg, e);
	}

}
