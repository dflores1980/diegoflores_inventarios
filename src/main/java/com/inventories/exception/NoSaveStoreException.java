package com.inventories.exception;

public class NoSaveStoreException extends Exception {

	private static final long serialVersionUID = -8289300744303871297L;

	public NoSaveStoreException(String msg, Exception e) {
		super(msg, e);
	}

}
