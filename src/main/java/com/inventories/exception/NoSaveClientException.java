package com.inventories.exception;

public class NoSaveClientException extends Exception {

	private static final long serialVersionUID = -8289300744303871297L;

	public NoSaveClientException(String msg, Exception e) {
		super(msg, e);
	}

}
