package com.inventories.exception;

public class NoSaveOrdersException extends Exception {

	private static final long serialVersionUID = -8289300744303871297L;

	public NoSaveOrdersException(String msg, Exception e) {
		super(msg, e);
	}

}
