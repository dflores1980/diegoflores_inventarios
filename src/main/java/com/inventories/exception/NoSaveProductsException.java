package com.inventories.exception;

public class NoSaveProductsException extends Exception {

	private static final long serialVersionUID = -8289300744303871297L;

	public NoSaveProductsException(String msg, Exception e) {
		super(msg, e);
	}

}
