package com.inventories.model;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product {

	@Id
	private Long id;

	private String cod;

	private String name;

	private BigDecimal price;

	private Integer stock;
	
	@ManyToMany(fetch = FetchType.LAZY)
    private Set<Store> stores;

	public Product(String cod, String name) {
		super();
		this.cod = cod;
		this.name = name;
	}

}
