package com.inventories.model.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class ProductTo implements Serializable {

	private static final long serialVersionUID = -3677455960012857440L;

	private Long id;

	private String cod;

	private String name;

	private BigDecimal price;

	private Integer stock;

}
