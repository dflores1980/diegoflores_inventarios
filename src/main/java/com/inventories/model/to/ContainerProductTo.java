package com.inventories.model.to;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContainerProductTo implements Serializable {

	private static final long serialVersionUID = 2318683449702254747L;

	private List<ProductTo> prods = null;

}