package com.inventories.model.to;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class OrderTo implements Serializable {

	private static final long serialVersionUID = 7443073574565246804L;

	private Long id;

	private StoreTo storeTo;

	private ClientTo clientTo;

	private ProductTo productTo;

	private Integer amount;

	private LocalDateTime dateOrder;

}
