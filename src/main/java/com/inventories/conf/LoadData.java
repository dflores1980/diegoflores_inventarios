package com.inventories.conf;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.inventories.dao.ProductRepository;
import com.inventories.exception.NoSaveProductsException;
import com.inventories.model.to.ProductTo;
import com.inventories.service.ProductService;
import com.inventories.service.client.ProductServiceClient;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class LoadData {

	@Autowired
	private ProductServiceClient productServiceClient;

	@Autowired
	private ProductService productService;

	@Bean
	CommandLineRunner initDatabase(ProductRepository productoRepositorio) {
		return args -> {
			log.info("BEGIN LOAD DATA");
			List<ProductTo> products = productServiceClient.getAllProducts();
			try {
				productService.saveManyProducts(products);
			} catch (NoSaveProductsException e) {
				log.error("ERROR SAVE PRODUCTS", e);
			}

			log.info("END LOAD DATA");
		};
	}
}
