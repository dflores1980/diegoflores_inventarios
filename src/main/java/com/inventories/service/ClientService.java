package com.inventories.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventories.dao.ClientRepository;
import com.inventories.exception.NoDeleteClientException;
import com.inventories.exception.NoSaveClientException;
import com.inventories.model.Client;
import com.inventories.model.to.ClientTo;

@Service
public class ClientService {

	@Autowired
	private ClientRepository clientRepository;

	public List<ClientTo> findClientsAll() {
		return getClientsTo(clientRepository.findAll());
	}

	public ClientTo saveOrUpdateClient(Client client) throws NoSaveClientException {
		try {
			if (client.getIdentification().trim().isEmpty() || client.getName().trim().isEmpty()) {
				throw new NoSaveClientException("Identification and Name for the client is required ", null);
			}
			client = clientRepository.save(client);
			return toClientTo(client);
		} catch (Exception e) {
			throw new NoSaveClientException(
					String.format("Client with id %s error &s  ", client.getId() == null ? "null" : client.getId()), e);
		}
	}

	public void deleteClient(Client client) throws NoDeleteClientException {
		try {
			clientRepository.delete(client);
		} catch (Exception e) {
			throw new NoDeleteClientException(
					String.format("Client with id %s error &s  ", client.getId() == null ? "null" : client.getId()), e);
		}
	}

	private List<ClientTo> getClientsTo(List<Client> clients) {
		return clients.stream().map(c -> toClientTo(c)).collect(Collectors.toList());
	}

	public ClientTo toClientTo(Client c) {
		if (c == null) {
			return null;
		}
		return new ClientTo(c.getId(), c.getName(), c.getIdentification(), c.getPhoto());
	}

	public Client toClient(ClientTo c) {
		if (c == null) {
			return null;
		}
		return new Client(c.getId(), c.getName(), c.getIdentification(), c.getPhoto());
	}

}
