package com.inventories.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventories.dao.OrderRepository;
import com.inventories.exception.NoSaveOrdersException;
import com.inventories.model.Order;
import com.inventories.model.to.OrderTo;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private ClientService clientService;

	@Autowired
	private StoreService storeService;

	@Autowired
	private ProductService productService;

	public List<OrderTo> getOrdersAll() {
		List<Order> orders = orderRepository.findAll();
		return getOrderTo(orders);
	}

	public List<OrderTo> saveManyOrders(List<OrderTo> orders) throws NoSaveOrdersException {
		List<OrderTo> result = new ArrayList<OrderTo>();
		for (OrderTo orderTo : orders) {
			Order order = toOrder(orderTo);
			try {
				order = orderRepository.save(order);
				OrderTo orderToSave = toOrderTo(order);
				result.add(orderToSave);
			} catch (Exception e) {
				throw new NoSaveOrdersException("The orders no save success ", e);
			}
		}
		return result;
	}

	private List<OrderTo> getOrderTo(List<Order> orders) {
		return orders.stream().map(o -> toOrderTo(o)).collect(Collectors.toList());
	}

	private OrderTo toOrderTo(Order o) {
		return new OrderTo(o.getId(), storeService.toStoreTo(o.getStore()), clientService.toClientTo(o.getClient()),
				productService.toProductTo(o.getProduct()), o.getAmount(), o.getDateOrder());

	}

	private Order toOrder(OrderTo o) {
		return new Order(o.getId(), storeService.toStore(o.getStoreTo()), clientService.toClient(o.getClientTo()),
				productService.toProduct(o.getProductTo(), null), o.getAmount(), o.getDateOrder());

	}

}
