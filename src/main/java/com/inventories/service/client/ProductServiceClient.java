package com.inventories.service.client;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.inventories.model.to.ContainerProductTo;
import com.inventories.model.to.ProductTo;

@Service
public class ProductServiceClient {

	@Autowired
	RestTemplate restTemplate;

	public List<ProductTo> getAllProducts() {
		ResponseEntity<ContainerProductTo> resp = restTemplate
				.getForEntity("https://mocki.io/v1/dab994cb-5b62-4647-8ea3-c145bd7ca3ce", ContainerProductTo.class);
		return resp.getStatusCode() == HttpStatus.OK ? resp.getBody().getProds() : new ArrayList<ProductTo>();
	}

}
