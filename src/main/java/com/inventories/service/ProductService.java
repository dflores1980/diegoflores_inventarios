package com.inventories.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventories.dao.ProductRepository;
import com.inventories.exception.NoSaveProductsException;
import com.inventories.model.Product;
import com.inventories.model.Store;
import com.inventories.model.to.ProductTo;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

//	private List<Product> getProductsEntity(List<ProductTo> products) {
//		return products.stream().map(p -> p.getEntity()).collect(Collectors.toList());
//	}

	private List<ProductTo> getProductsTo(List<Product> products) {
		return products.stream().map(p -> toProductTo(p)).collect(Collectors.toList());
	}

	public void saveManyProducts(List<ProductTo> products) throws NoSaveProductsException {
		for (ProductTo productTo : products) {
			Product product = toProduct(productTo, null);
			try {
				productRepository.save(product);
			} catch (Exception e) {
				throw new NoSaveProductsException(String.format("Product with id %s error &s  ", product.getId()), e);
			}
		}
	}

	public List<ProductTo> findProductsAllCodName() {
		return getProductsTo(productRepository.findProductsAllCodName());
	}

	public ProductTo updateStock(Long id, Integer stock) throws NoSaveProductsException {
		if (stock <= 0) {
			throw new NoSaveProductsException("Stock of the product must be greater than zero", null);
		}
		Product product = productRepository.findById(id).orElseThrow(
				() -> new NoSaveProductsException(String.format("Product not found for id %s ", id), null));
		product.setStock(stock);
		try {
			product = productRepository.save(product);
		} catch (Exception e) {
			throw new NoSaveProductsException(String.format("Product update for id %s error %s ", product.getId()), e);
		}
		return toProductTo(product);
	}

	public ProductTo toProductTo(Product p) {
		if (p == null) {
			return null;
		}
		return new ProductTo(p.getId(), p.getCod(), p.getName(), p.getPrice(), p.getStock());
	}

	public Product toProduct(ProductTo p, Set<Store> stores) {
		return new Product(p.getId(), p.getCod(), p.getName(), p.getPrice(), p.getStock(), stores);
	}
}
