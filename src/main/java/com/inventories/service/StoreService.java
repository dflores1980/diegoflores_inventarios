package com.inventories.service;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventories.dao.ProductRepository;
import com.inventories.dao.StoreRepository;
import com.inventories.exception.NoSaveStoreException;
import com.inventories.model.Product;
import com.inventories.model.Store;
import com.inventories.model.to.StoreTo;

@Service
public class StoreService {

	@Autowired
	private StoreRepository storeRepository;

	@Autowired
	private ProductRepository productRepository;

	public StoreTo saveStoreProduct(String codStore, String codProduct) throws NoSaveStoreException {
		Store store = storeRepository.findStoreByCode(codStore);
		Product product = productRepository.findProductByCode(codProduct);
		if (store == null || product == null) {
			throw new NoSaveStoreException("Store or Product no exist", null);
		}
		store.setProducts(new HashSet<Product>());
		store.getProducts().add(product);
		try {
			store = storeRepository.save(store);
			return toStoreTo(store);
		} catch (Exception e) {
			throw new NoSaveStoreException(String.format("Store with id %s error &s  ", store.getId()), e);
		}
	}

	public StoreTo toStoreTo(Store s) {
		if (s == null) {
			return null;
		}
		return new StoreTo(s.getId(), s.getCod(), s.getName());
	}

	public Store toStore(StoreTo s) {
		if (s == null) {
			return null;
		}
		return new Store(s.getId(), s.getCod(), s.getName(), null);
	}

}
