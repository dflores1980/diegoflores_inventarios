package com.inventories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inventories.model.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

}
