package com.inventories.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.inventories.model.Store;

public interface StoreRepository extends JpaRepository<Store, Long> {

	@Query(value = "SELECT s FROM Store s WHERE s.cod = ?1")
	Store findStoreByCode(String code);

}
