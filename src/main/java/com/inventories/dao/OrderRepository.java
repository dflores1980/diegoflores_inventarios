package com.inventories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inventories.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

}
