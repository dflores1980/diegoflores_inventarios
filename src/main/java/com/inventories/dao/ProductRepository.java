package com.inventories.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.inventories.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query(value = "SELECT new com.inventories.model.Product(p.cod, p.name) FROM Product p")
	List<Product> findProductsAllCodName();

	@Query(value = "SELECT p FROM Product p WHERE p.cod = ?1")
	Product findProductByCode(String code);

}
