package com.inventories.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inventories.exception.NoSaveOrdersException;
import com.inventories.model.to.OrderTo;
import com.inventories.service.OrderService;

@RestController
@RequestMapping("/api/orders/")
public class OrdersController {

	@Autowired
	private OrderService orderService;

	@GetMapping("/getOrdersAll")
	public List<OrderTo> getOrdersAll() {
		List<OrderTo> orders = orderService.getOrdersAll();
		return orders;

	}

	@PutMapping("/saveOrUpdateOrder")
	public ResponseEntity<List<OrderTo>> saveOrUpdateOrder(@RequestBody List<OrderTo> orders) {
		List<OrderTo> resulList = new ArrayList<OrderTo>();
		try {
			resulList.addAll(orderService.saveManyOrders(orders));
			return new ResponseEntity<List<OrderTo>>(resulList, HttpStatus.ACCEPTED);
		} catch (NoSaveOrdersException e) {
			return new ResponseEntity<List<OrderTo>>(resulList, HttpStatus.PRECONDITION_FAILED);
		}
	}

}
