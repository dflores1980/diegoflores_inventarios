package com.inventories.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inventories.exception.NoSaveStoreException;
import com.inventories.model.to.StoreTo;
import com.inventories.service.StoreService;

@RestController
@RequestMapping("/api/stores/")
public class StoresController {

	@Autowired
	private StoreService storeService;

	@PostMapping("/createRelation/{codStore}/{codProduct}")
	public ResponseEntity<StoreTo> createRelation(@PathVariable String codStore, @PathVariable String codProduct) {
		try {
			StoreTo store = storeService.saveStoreProduct(codStore, codProduct);
			return ResponseEntity.ok(store);
		} catch (NoSaveStoreException e) {
			return new ResponseEntity<StoreTo>(HttpStatus.PRECONDITION_FAILED);
		}
	}

}
