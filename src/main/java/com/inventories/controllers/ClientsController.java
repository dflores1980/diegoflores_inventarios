package com.inventories.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inventories.exception.NoDeleteClientException;
import com.inventories.exception.NoSaveClientException;
import com.inventories.model.Client;
import com.inventories.model.to.ClientTo;
import com.inventories.service.ClientService;

@RestController
@RequestMapping("/api/clients/")
public class ClientsController {

	@Autowired
	private ClientService clientService;

	@GetMapping("/getClientsAll")
	public List<ClientTo> getClientsAll() {
		List<ClientTo> clients = clientService.findClientsAll();
		return clients;
	}

	@PutMapping("/saveOrUpdateClient")
	public ResponseEntity<ClientTo> updateClient(@RequestBody Client client) {
		try {
			ClientTo clientTo = clientService.saveOrUpdateClient(client);
			return ResponseEntity.ok(clientTo);
		} catch (NoSaveClientException e) {
			return new ResponseEntity<ClientTo>(HttpStatus.PRECONDITION_FAILED);
		}
	}

	@DeleteMapping("/deleteClient")
	public ResponseEntity<String> deleteClient(@RequestBody Client client) {
		try {
			clientService.deleteClient(client);
			return new ResponseEntity<String>("Exito", HttpStatus.ACCEPTED);
		} catch (NoDeleteClientException e) {
			return new ResponseEntity<String>("Error", HttpStatus.PRECONDITION_FAILED);
		}
	}

}
