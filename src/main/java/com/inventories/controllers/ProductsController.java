package com.inventories.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inventories.exception.NoSaveProductsException;
import com.inventories.model.to.ProductTo;
import com.inventories.service.ProductService;

@RestController
@RequestMapping("/api/products/")
public class ProductsController {

	@Autowired
	private ProductService productService;

	@GetMapping("/getProductsAllCodName")
	public List<ProductTo> getProductsAllCodName() {
		List<ProductTo> productos = productService.findProductsAllCodName();
		return productos;
	}

	@PutMapping("/updateStock/{id}/{stock}")
	public ResponseEntity<ProductTo> updateStock(@PathVariable Long id, @PathVariable Integer stock) {
		try {
			ProductTo productTo = productService.updateStock(id, stock);
			return ResponseEntity.ok(productTo);
		} catch (NoSaveProductsException e) {
			// ApiError apiError = new ApiError(HttpStatus.PRECONDITION_FAILED, "Validation
			// error", e.getMessage());
			// return new ResponseEntity<ProductTo>(apiError, apiError.getStatus());
			return new ResponseEntity<ProductTo>(HttpStatus.PRECONDITION_FAILED);
			// return
			// ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
		}

	}

}
