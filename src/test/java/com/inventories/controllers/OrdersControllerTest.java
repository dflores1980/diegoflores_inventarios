package com.inventories.controllers;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.inventories.model.to.ClientTo;
import com.inventories.model.to.OrderTo;
import com.inventories.model.to.ProductTo;
import com.inventories.model.to.StoreTo;

@SpringBootTest
@AutoConfigureMockMvc
public class OrdersControllerTest {

	@Autowired
	private OrdersController controller;

	@Test
	public void controllerLoad() throws Exception {
		Assertions.assertThat(controller).isNotNull();
	}

	@Test
	public void saveOrUpdateOrder() throws Exception {
		List<OrderTo> origen = getOrders();
		ResponseEntity<List<OrderTo>> resp = controller.saveOrUpdateOrder(origen);
		assertThat(origen.size()).isEqualTo(resp.getBody().size());
	}

	private List<OrderTo> getOrders() {
		List<OrderTo> os = new ArrayList<>();
		os.add(getOrder(10));
		os.add(getOrder(20));
		os.add(getOrder(30));
		return os;
	}

	private OrderTo getOrder(Integer amount) {
		OrderTo o = new OrderTo();
		o.setClientTo(new ClientTo(1l, "Name", "172365254", null));
		o.setProductTo(new ProductTo(1l, "Cod", "Name", BigDecimal.TEN, 200));
		o.setStoreTo(new StoreTo(1l, "Cod", "Name"));
		o.setAmount(amount);
		o.setDateOrder(LocalDateTime.now());
		return o;
	}

}
