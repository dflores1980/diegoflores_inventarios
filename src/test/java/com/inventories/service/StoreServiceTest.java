package com.inventories.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.inventories.exception.NoSaveStoreException;
import com.inventories.model.to.StoreTo;

@SpringBootTest
@AutoConfigureMockMvc
public class StoreServiceTest {

	@Autowired
	private StoreService storeService;

	@Test
	void testCreateStore() {
		StoreTo storeTo = null;
		try {
			storeTo = storeService.saveStoreProduct(Mockito.anyString(), Mockito.anyString());
		} catch (NoSaveStoreException e) {
			Assert.isNull(storeTo, "Store no create");
			return;
		}
		Assert.notNull(storeTo, "Store create");
	}

}
