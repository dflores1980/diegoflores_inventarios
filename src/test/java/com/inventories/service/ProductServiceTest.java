package com.inventories.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.inventories.exception.NoSaveProductsException;
import com.inventories.model.to.ProductTo;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductServiceTest {

	@Autowired
	private ProductService service;

	@Test
	void testCreateProduct() throws NoSaveProductsException {
		ProductTo productTo = service.updateStock(1l, 10);
		Assert.notNull(productTo, "Product created");
	}

}
