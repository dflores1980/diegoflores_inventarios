package com.inventories.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.inventories.exception.NoSaveClientException;
import com.inventories.model.Client;
import com.inventories.model.to.ClientTo;

@SpringBootTest
@AutoConfigureMockMvc
public class ClientServiceTest {

	@Autowired
	private ClientService clientService;

	@Test
	void testCreateClient() throws NoSaveClientException {
		ClientTo clientTo = clientService.saveOrUpdateClient(getClient());
		Assert.notNull(clientTo, "Client created");
	}

	private Client getClient() {
		Client o = new Client();
		o.setIdentification("1712566589");
		o.setName("Name");
		return o;
	}

}
