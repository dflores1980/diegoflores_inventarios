# diegoFlores_inventarios

Caso práctivo planteado para inventarios

## Getting started

## PREREQUISITOS

1.- Descargar el proyectos del GIT ubicado en el ruta:  https://gitlab.com/dflores1980/diegoflores_inventarios.git

2.- Installar MySQL

3.- Crear la base de datos llamada "inventory"

4.- Configurar la conexión a la base de datos en el archivo application.properties en el proyecto (de querer cambiar de motor de base o nombre también)

## INICIAR

1.- En la carpeta del proyecto ejecutar: mvn spring-boot:run

En esta ocasión se va a crear la estructura de base de base de datos, también se cargarán los datos de la tabla "product"

2.- Bajar el servidor tomcat con el proyecto

3.- En la carpeta del proyecto ejecutar: mvn spring-boot:run

En esta ocasión se creará los datos ejecutados del script data.sql para llena la tabla "store"

4.- En la carpeta del proyecto ejecutar: mvn clean install

Esto compilará el proyecto y se creará los datos de la tabla client y order_client con la prueba de integración (1)

También se podrá validar las pruebas unitarias (3)

## VALIDAR REQUERIMIENTOS

Se indica los endpoints creados:

1.- Para el servicio Mock de carga de productos

https://mocki.io/v1/dab994cb-5b62-4647-8ea3-c145bd7ca3ce

2.- Para obtener todos los productos (Solo cod y name)

localhost:8090/api/products/getProductsAllCodName

3.- Para actualizar stock de un producto

localhost:8090/api/products/updateStock/1/10

4.- Para crear la relación entre la tienda y el producto

localhost:8090/api/stores/createRelation/store-2/prod-2

5.- Servicios para CRUD de client

5.1.- Para obtener todos los clientes
localhost:8090/api/clients/getClientsAll

5.2.- Para insertar o actualizar el cliente
localhost:8090/api/clients/saveOrUpdateClient

Colocar en el BODY como Json

## Para crear
	
    {
        "id": null,
        "name": "Nuevo Diego",
        "identification": "172356589",
        "photo": null
    }

## Para actualizar    
	
    {
        "id": 1,
        "name": "Nuevo Diego",
        "identification": "172356589",
        "photo": null
    }

5.3.- Para eliminar el cliente
localhost:8090/api/clients/deleteClient
	
    {
        "id": 1,
        "name": "Nuevo Diego",
        "identification": "1716632680001",
        "photo": null
    }

6.- Servicio para obtener todas las ordenes
localhost:8090/api/orders/getOrdersAll

7.- Servicio para el registro o actualización de la orden (Pedido), para esto se probo con prubas de integración (Clase:OrdersControllerTest)
localhost:8090/api/orders/saveOrUpdateOrder
